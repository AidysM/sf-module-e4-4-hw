FROM python:3.9

ENV LANG C.UTF-8
ENV USER=app
ENV PROJECTPATH=/home/${USER}/flask

RUN set -x \
    && apt-get update && apt-get -y upgrade \
#    && apt-get install -y --no-install-recommends \
#      libpq-dev git \
    && apt-get purge -y --auto-remove \
    && rm -rf /var/lib/apt/lists/*

RUN useradd -d /home/${USER} ${USER} \
    && mkdir -p /home/${USER}/logs/ \
    && chown -R ${USER} /home/${USER}

COPY ./requirements.txt ${PROJECTPATH}/

RUN pip install --upgrade pip \
    && pip install --no-cache-dir -r ${PROJECTPATH}/requirements.txt

COPY . ${PROJECTPATH}

WORKDIR ${PROJECTPATH}/src

USER ${USER}
