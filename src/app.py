import os
from flask import Flask, request, render_template

from utils import get_favicon


app = Flask(__name__)

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
DEFAULT_URL = ['https://wiki.cs.money/ru', 'https://ru.wikipedia.org']
FAVICONS = []


@app.route('/', methods=['GET', 'POST'])
def root_view():
    favicons = FAVICONS
    if request.method == 'POST':
        favicon_data = get_favicon(request.form['url'])
        if len(favicons) <= 1:
            for f in favicons:
                if favicon_data.get('site_url') != f.get('site_url'):
                    favicons.append(favicon_data)
        else:
            favicons.append(favicon_data)
    print("F", favicons)

    return render_template('index.html', favicons=favicons)


if __name__ == '__main__':
    app.run(host='0.0.0.0')


