import os
import requests
from bs4 import BeautifulSoup
from urllib.parse import urlparse

STATIC_DIR = os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), 'static')
STATIC_URL = "/static"


def get_favicon(url) -> dict:
    domain_name = "_".join(urlparse(url).netloc.split(".")[-2:])
    if not os.path.exists(os.path.join(STATIC_DIR, domain_name)):
        os.makedirs(os.path.join(STATIC_DIR, domain_name), mode=0o777)

    resp = requests.get(url).text
    soup = BeautifulSoup(resp, 'html5lib')
    favicon_link = soup.find_all(["meta", "link"], rel="icon")[0].get("href")
    favicon_url = url + favicon_link

    favicon_name = favicon_link.split('/')[-1:][0]

    r = requests.get(favicon_url)
    with open(os.path.join(STATIC_DIR, domain_name, favicon_name), 'wb') as f:
        f.write(r.content)

    return {"site_url": url, "image": f"{STATIC_URL}/{domain_name}/{favicon_name}"}
